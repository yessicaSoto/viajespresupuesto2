namespace DBViajesP
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class Contexto : DbContext
    {
        public Contexto()
            : base("name=Contexto")
        {
        }

        public virtual DbSet<CASETAS> CASETAS { get; set; }
        public virtual DbSet<CIUDADES> CIUDADES { get; set; }
        public virtual DbSet<COSTO_CASETA> COSTO_CASETA { get; set; }
        public virtual DbSet<ESTADO> ESTADO { get; set; }
        public virtual DbSet<ROLES> ROLES { get; set; }
        public virtual DbSet<RUTAS> RUTAS { get; set; }
        public virtual DbSet<TIPO_COMBUSTIBLE> TIPO_COMBUSTIBLE { get; set; }
        public virtual DbSet<TIPO_TRANSPORTE> TIPO_TRANSPORTE { get; set; }
        public virtual DbSet<UNIDADES> UNIDADES { get; set; }
        public virtual DbSet<USUARIOS> USUARIOS { get; set; }
        public virtual DbSet<VIAJES> VIAJES { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<CASETAS>()
                .HasMany(e => e.COSTO_CASETA)
                .WithRequired(e => e.CASETAS)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<CASETAS>()
                .HasMany(e => e.RUTAS)
                .WithMany(e => e.CASETAS)
                .Map(m => m.ToTable("RUTA_CASETAS").MapLeftKey("ID_CASETA").MapRightKey("ID_RUTA"));

            modelBuilder.Entity<CIUDADES>()
                .Property(e => e.CIUDAD)
                .IsUnicode(false);

            modelBuilder.Entity<CIUDADES>()
                .HasMany(e => e.VIAJES)
                .WithRequired(e => e.CIUDADES)
                .HasForeignKey(e => new { e.ID_CIUDAD_PARTIDA, e.ID_ESTADO_PARTIDA })
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<CIUDADES>()
                .HasMany(e => e.VIAJES1)
                .WithRequired(e => e.CIUDADES1)
                .HasForeignKey(e => new { e.ID_CIUDAD_DESTINO, e.ID_ESTADO_DESTINO })
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<CIUDADES>()
                .HasMany(e => e.RUTAS)
                .WithMany(e => e.CIUDADES)
                .Map(m => m.ToTable("RUTAS_DESTINOS").MapLeftKey(new[] { "ID_CIUDAD", "ID_ESTADO" }).MapRightKey("ID_RUTA"));

            modelBuilder.Entity<COSTO_CASETA>()
                .Property(e => e.COSTO)
                .HasPrecision(18, 0);

            modelBuilder.Entity<ESTADO>()
                .Property(e => e.ESTADO1)
                .IsUnicode(false);

            modelBuilder.Entity<ESTADO>()
                .HasMany(e => e.CIUDADES)
                .WithRequired(e => e.ESTADO)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<RUTAS>()
                .Property(e => e.DISTANCIA)
                .HasPrecision(18, 0);

            modelBuilder.Entity<RUTAS>()
                .HasMany(e => e.VIAJES)
                .WithMany(e => e.RUTAS)
                .Map(m => m.ToTable("VIAJES_RUTA").MapLeftKey("ID_RUTA").MapRightKey("ID_VIAJE"));

            modelBuilder.Entity<TIPO_COMBUSTIBLE>()
                .Property(e => e.DESCRIPCION)
                .IsUnicode(false);

            modelBuilder.Entity<TIPO_COMBUSTIBLE>()
                .Property(e => e.COSTO_LITRO)
                .HasPrecision(18, 0);

            modelBuilder.Entity<TIPO_COMBUSTIBLE>()
                .HasMany(e => e.UNIDADES)
                .WithRequired(e => e.TIPO_COMBUSTIBLE)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<TIPO_TRANSPORTE>()
                .Property(e => e.DESCRIPCION)
                .IsUnicode(false);

            modelBuilder.Entity<TIPO_TRANSPORTE>()
                .HasMany(e => e.COSTO_CASETA)
                .WithRequired(e => e.TIPO_TRANSPORTE)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<TIPO_TRANSPORTE>()
                .HasMany(e => e.UNIDADES)
                .WithRequired(e => e.TIPO_TRANSPORTE)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<UNIDADES>()
                .HasMany(e => e.VIAJES)
                .WithMany(e => e.UNIDADES)
                .Map(m => m.ToTable("UNIDADES_VIAJE").MapLeftKey("ID_UNIDAD").MapRightKey("ID_VIAJE"));

            modelBuilder.Entity<USUARIOS>()
                .Property(e => e.PASSWORD)
                .IsFixedLength();

            modelBuilder.Entity<USUARIOS>()
                .HasMany(e => e.ROLES)
                .WithMany(e => e.USUARIOS)
                .Map(m => m.ToTable("USUARIOS_ROL").MapLeftKey("ID_USUARIO").MapRightKey("ID_ROL"));
        }
    }
}
