﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace POCOS.Catalogos
{
    public class mViaje
    {
        public int ID_VIAJE { get; set; }

        public bool ES_REDONDO { get; set; }

        public int CANTIDAD_PASEJEROS { get; set; }

        public DateTime FECHA_PARTIDAD { get; set; }

        public DateTime FECHA_RETORNO { get; set; }

        public TimeSpan HORA_PARTIDAD { get; set; }

        public TimeSpan HORA_RETORNO { get; set; }

        public int ID_RUTA { get; set; }

        public string ruta { get; set; }
        public string nombreRuta { get; set; }
        public string tipo { get; set; }
        public string fechaPar { get; set; }
        public string horaPar { get; set; }
        public string FechaRe { get; set; }
        public string horRe { get; set; }




    }
}
