﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace POCOS.Catalogos
{
    public class mTipoTransporte
    {
        public int ID_TIPO_TRANSPORTE { get; set; }

        public string TIPO_TRANSPORTE { get; set; }
    }
}
