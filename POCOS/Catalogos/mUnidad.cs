﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace POCOS.Catalogos
{
    public class mUnidad
    {
        public int ID_UNIDAD { get; set; }

        public string UNIDAD { get; set; }

        public mTipoCombustible COMBUSTIBLE { get; set; }
        
        public double costoCombustible { get; set; }
        public string tipoCombustible { get; set; }

        public int ID_TRANSPORTE { get; set; }

        public string COMBUSTIBLEDES { get; set; }

        public string TRANSPORTEDES { get; set; }

        public int CAPACIDAD { get; set; }

        public string DISPONIBLE { get; set; }

        public bool  ESTA_DISPONIBLE { get; set; }
        public double kilometraje { get; set; }

    }
}
