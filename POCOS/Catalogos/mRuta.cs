﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace POCOS.Catalogos
{
    public class mRuta
    {
        public int ID_RUTA { get; set; }

        public string RUTA { get; set; }

        public int ID_CIUDAD_PARTIDA { get; set; }

        public double DISTANCIA { get; set; }

        public List<mCiudad> DESTINOS { get; set; }

        public List<int> Casetas { get; set; }

        public string destinosDesc { get; set; }
        public string ciudadPartidaDesc { get; set; }

    }
}
