﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace POCOS.Catalogos
{
    public class mCaseta
    {
        public int ID_CASETA { get; set; }

        public double COSTO { get; set; }

        public string UBICACION { get; set; }

        public string NOMBRE { get; set; }

        public List<mRuta> RUTAS { get; set; }

        public string RUTASDESC { get; set; }
        public List<mCostoCaseta> costoCasetas { get; set; }
    }
}
