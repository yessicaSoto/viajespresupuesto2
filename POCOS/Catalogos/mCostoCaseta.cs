﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace POCOS.Catalogos
{
    public class mCostoCaseta
    {
        public int ID_RUTA { get; set; }
        public int ID_CASETA { get; set; }
        public int ID_TIPO_TRANSPORTE { get; set; }
        public string caseta { get; set; }
        public double COSTO{ get; set; }
        public string tipoTransporte { get; set; }
        public string unidad { get; set; }
    }
}
