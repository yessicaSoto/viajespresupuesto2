﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace POCOS.Catalogos
{
    public class mCiudad
    {
        public int ID_CIUDAD { get; set; }

        public int ID_ESTADO { get; set; }

        public string CIUDAD { get; set; }

        public bool ES_SALIDA { get; set; }

        public bool ES_DESTINO { get; set; }
    }
}
