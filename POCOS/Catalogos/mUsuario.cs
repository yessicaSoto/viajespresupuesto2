﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace POCOS.Catalogos
{
    public class mUsuario
    {
        public int ID_USUARIO { get; set; }

        public string USUARIO { get; set; }

        public List<int> Roles { get; set; }
    }
}
