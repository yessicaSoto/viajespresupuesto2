﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace POCOS.Catalogos
{
    public class mPresupuesto
    {
       public mViaje DatosGenerales { get; set; }
        public string ciudadPartida { get; set; }
        public string ciudadDestino { get; set; }

        public List<mCostoCaseta> peajes { get; set; }
       public Double totalPeajes { get; set; }
       public List<mTipoCombustible> costosCombustible { get; set; }
        public Double viaticos { get; set; }
        public Double total { get; set; }




    }
}
