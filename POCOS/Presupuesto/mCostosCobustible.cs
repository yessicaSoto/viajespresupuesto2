﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace POCOS.Catalogos
{
    public class mCostorCombustible
    {
        public int ID_TIPO_COMBUSTIBLE { get; set; }

        public string TIPO_COMBUSTIBLE { get; set; }

        public double COSTO_LITRO { get; set; }
        public string COSTO_LITRO_DESC { get; set; }

        int ID_UNIDAD { get; set; }

    }
}
