﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using POCOS.Catalogos;

namespace Controlador.Interfaces
{
    public interface IRutas
    {
        IQueryable<mRuta> getRuta(string nombreRuta);
        IQueryable<mRuta> Listado();
    }
}
