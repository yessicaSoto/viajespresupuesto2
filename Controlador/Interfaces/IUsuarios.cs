﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using POCOS.Catalogos;

namespace Controlador.Interfaces
{
    public interface IUsuarios
    {
        IQueryable<mUsuario> getUsuario(string nombreUsuario);
    }
}
