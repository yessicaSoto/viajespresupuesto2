﻿using Controlador.Tools;

using DBViajesP;
using POCOS.Catalogos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace Controlador.Catalogos
{
    public class cRutas : EntityManagerServer<RUTAS>
    {

        public List<mRuta> Listado()
        {
            try
            {
                
                Context.Configuration.LazyLoadingEnabled = true;
                var query = GetData().Select(w => new mRuta()
                {
                    ID_RUTA = w.ID_RUTA,
                    RUTA = w.NOMBRE,
                    //destinosDesc = w.CIUDADES1.Any() ? string.Join(", ", w.CIUDADES1.Select(c => c.CIUDAD).ToList()) : string.Empty,
                    DESTINOS = w.CIUDADES.Select( c=> new mCiudad() {
                        ID_CIUDAD = c.ID_CIUDAD,
                        CIUDAD = c.CIUDAD
                    }).ToList()

                }).OrderBy(w => w.ID_RUTA);

                var listado = query != null ? query.ToList() : new List<mRuta>();
                foreach (var i in listado)
                {
                    i.destinosDesc = i.DESTINOS.Any() ?
                        string.Join(", ", i.DESTINOS.Select(c => c.CIUDAD).ToList()) : string.Empty;
                }
                
                return listado;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<mRuta> ListadoConSalidaYDestino(int cSalida, int cDestino)
        {
            try
            {
      
                Context.Configuration.LazyLoadingEnabled = true;
                var query = GetData().Where(w=> w.CIUDADES.Any(a=> a.ID_CIUDAD == cSalida)
                && w.CIUDADES.Any(a => a.ID_CIUDAD == cDestino)).Select(w => new mRuta()
                {
                    ID_RUTA = w.ID_RUTA,
                    RUTA = w.NOMBRE,
                    DESTINOS = w.CIUDADES.Select(c => new mCiudad()
                    {
                        ID_CIUDAD = c.ID_CIUDAD,
                        CIUDAD = c.CIUDAD
                    }).ToList()

                }).OrderBy(w => w.ID_RUTA);

                var listado = query != null ? query.ToList() : new List<mRuta>();
                foreach (var i in listado)
                {
                    i.destinosDesc = i.DESTINOS.Any() ?
                        string.Join(", ", i.DESTINOS.Select(c => c.CIUDAD).ToList()) : string.Empty;
                }

                return listado;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public mRuta ObtenerRuta(int ruta)
        {
            try
            {

                Context.Configuration.LazyLoadingEnabled = true;
                var datosRuta = Context.RUTAS.FirstOrDefault(w => w.ID_RUTA == ruta);
                var desc = datosRuta.CIUDADES.Any() && datosRuta.CIUDADES != null ? string.Join(", ", datosRuta.CIUDADES.Select(c => c.CIUDAD).ToList()) : string.Empty;

               return new mRuta
                {
                    destinosDesc = desc,
                    RUTA = datosRuta.NOMBRE,
                    DISTANCIA = (double)datosRuta.DISTANCIA
                };

                
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
