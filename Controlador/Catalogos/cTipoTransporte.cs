﻿using Controlador.Tools;

using DBViajesP;
using POCOS.Catalogos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace Controlador.Catalogos
{
    public class cTipoTransporte : EntityManagerServer<TIPO_TRANSPORTE>
    {
        

        public List<mTipoTransporte> Listado()
        {
            try
            {
                var query = GetData().Select(w => new mTipoTransporte()
                {
                    ID_TIPO_TRANSPORTE = w.ID_TIPO_TRANSPORTE,
                    TIPO_TRANSPORTE = w.DESCRIPCION
                }).OrderBy(w => w.ID_TIPO_TRANSPORTE);

                var listado = query != null ? query.ToList() : new List<mTipoTransporte>();

                return listado;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<mTipoTransporte> ListadoPorCapacidad(int cantidad)
        {
            try
            {
                var query = GetData(w => w.UNIDADES.Any(c => c.CAPACIDAD >= cantidad)).Select(w => new mTipoTransporte()
                {
                    ID_TIPO_TRANSPORTE = w.ID_TIPO_TRANSPORTE,
                    TIPO_TRANSPORTE = w.DESCRIPCION
                }).OrderBy(w => w.ID_TIPO_TRANSPORTE);

                var listado = query != null ? query.ToList() : new List<mTipoTransporte>();

                return listado;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
