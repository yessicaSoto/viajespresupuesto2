﻿using Controlador.Tools;

using DBViajesP;
using POCOS.Catalogos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace Controlador.Catalogos
{
    public class cCostosCombustible : EntityManagerServer<TIPO_COMBUSTIBLE>
    {

        public List<mTipoCombustible> Listado()
        {
            try
            {
                
                Context.Configuration.LazyLoadingEnabled = true;

                var query = GetData().Select(w => new mTipoCombustible()
                {
                    ID_TIPO_COMBUSTIBLE = w.ID_TIPO_COMBUSTIBLE,
                    TIPO_COMBUSTIBLE = w.DESCRIPCION,
                    COSTO_LITRO = (double)w.COSTO_LITRO
                }).OrderBy(w => w.ID_TIPO_COMBUSTIBLE);

                var listado = query != null ? query.ToList() : new List<mTipoCombustible>();

                return listado;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    
    }
}
