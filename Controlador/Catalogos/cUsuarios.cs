﻿using Controlador.Tools;

using DBViajesP;
using POCOS.Catalogos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace Controlador.Catalogos
{
    public class cUsuarios : EntityManagerServer<USUARIOS>
    {
        public IQueryable<mUsuario> getUsuario(string nombreUsuario)
        {
            try
            {
                return GetData().Any(w => w.NOMBRE == nombreUsuario) ?
                    GetData(w => w.NOMBRE == nombreUsuario).Select(w => new mUsuario()
                    {
                        ID_USUARIO = w.ID_USUARIO,
                        USUARIO = w.NOMBRE
                    }).OrderBy(w => w.ID_USUARIO) :
                null;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
