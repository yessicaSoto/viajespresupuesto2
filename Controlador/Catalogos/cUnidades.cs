﻿using Controlador.Tools;

using DBViajesP;
using POCOS.Catalogos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace Controlador.Catalogos
{
    public class cUnidades : EntityManagerServer<UNIDADES>
    {

        public List<mUnidad> Listado()
        {
            try
            {
                Context.Configuration.LazyLoadingEnabled = true;
                var query = GetData().Select(w => new mUnidad()
                {
                    ID_UNIDAD = w.ID_UNIDAD,
                    TRANSPORTEDES = w.TIPO_TRANSPORTE != null ? w.TIPO_TRANSPORTE.DESCRIPCION : string.Empty,
                    COMBUSTIBLEDES = w.TIPO_COMBUSTIBLE != null ? w.TIPO_COMBUSTIBLE.DESCRIPCION : string.Empty,
                    CAPACIDAD = w.CAPACIDAD,
                    DISPONIBLE = w.ESTA_DISPONIBLE ? "si" : "No",

                }).OrderBy(w => w.ID_UNIDAD);
                var q = Context.TIPO_COMBUSTIBLE.FirstOrDefault().DESCRIPCION;
                var listado = query != null ? query.ToList() : new List<mUnidad>();

                //foreach(var i in listado)
                //{
                //    listado.
                //}

                return listado;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<mUnidad> ObtenerUnidades(int cantidad)
        {
            try
            {
                Context.Configuration.LazyLoadingEnabled = true;
                var unidades = Context.UNIDADES.Where(w => w.ESTA_DISPONIBLE == true).Select(w => new mUnidad()
                {
                    ID_UNIDAD = w.ID_UNIDAD,
                    CAPACIDAD = w.CAPACIDAD,
                    ID_TRANSPORTE = w.ID_TIPO_TRANSPORTE,
                    ESTA_DISPONIBLE = w.ESTA_DISPONIBLE
                }).OrderBy(w => w.CAPACIDAD);
                List<mUnidad> unidadesSeleccionadas = new List<mUnidad>();

                int restante = cantidad;
                do
                {
                    mUnidad unidad = new mUnidad() { };
                    if (Context.UNIDADES.Any(w => w.CAPACIDAD >= restante && w.ESTA_DISPONIBLE == true))
                    {

                        unidad = Context.UNIDADES.Select(w => new mUnidad()
                        {
                            ID_UNIDAD = w.ID_UNIDAD,
                            CAPACIDAD = w.CAPACIDAD,
                            ID_TRANSPORTE = w.ID_TIPO_TRANSPORTE
                        }).FirstOrDefault(w => w.CAPACIDAD >= cantidad);

                    }
                    else
                    {
                        int max = Context.UNIDADES.Where(w => w.ESTA_DISPONIBLE == true).Select(w => w.CAPACIDAD).Max();
                        unidad = Context.UNIDADES.Select(w => new mUnidad()
                        {
                            ID_UNIDAD = w.ID_UNIDAD,
                            CAPACIDAD = w.CAPACIDAD,
                            ID_TRANSPORTE = w.ID_TIPO_TRANSPORTE
                        }).FirstOrDefault(w => w.CAPACIDAD == max && w.ESTA_DISPONIBLE == true);

                        unidades.FirstOrDefault(w => w.ID_UNIDAD == unidad.ID_UNIDAD).ESTA_DISPONIBLE = false;
                    }
                    unidadesSeleccionadas.Add(unidad);
                    restante = restante - cantidad;
                } while (restante > 0);

                return unidadesSeleccionadas;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<mUnidad> ObtenerCostosCombustible(List<mUnidad> unidades)
        {
            try
            {
                foreach (var u in unidades)
                {
                    u.costoCombustible = Context.UNIDADES.Any(w => w.ID_UNIDAD == u.ID_UNIDAD) ?
                        Context.UNIDADES.FirstOrDefault(w => w.ID_UNIDAD == u.ID_UNIDAD).TIPO_COMBUSTIBLE != null
                        ? (double)Context.UNIDADES.FirstOrDefault(w => w.ID_UNIDAD == u.ID_UNIDAD).TIPO_COMBUSTIBLE.COSTO_LITRO
                        : 0 : 0;
                    u.tipoCombustible = Context.UNIDADES.Any(w => w.ID_UNIDAD == u.ID_UNIDAD) ?
                        Context.UNIDADES.FirstOrDefault(w => w.ID_UNIDAD == u.ID_UNIDAD).TIPO_COMBUSTIBLE != null
                        ? Context.UNIDADES.FirstOrDefault(w => w.ID_UNIDAD == u.ID_UNIDAD).TIPO_COMBUSTIBLE.DESCRIPCION
                        : string.Empty : string.Empty;
                }


                return unidades;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
