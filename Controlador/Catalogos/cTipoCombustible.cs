﻿using Controlador.Tools;
using Controlador.Interfaces;
using DBViajesP;
using POCOS.Catalogos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace Controlador.Catalogos
{
    public class cTipoCombustible : EntityManagerServer<TIPO_COMBUSTIBLE>
    {

        public List<mTipoCombustible> Listado()
        {
            try
            {
                var query = GetData().Select(w => new mTipoCombustible()
                {
                    ID_TIPO_COMBUSTIBLE = w.ID_TIPO_COMBUSTIBLE,
                    TIPO_COMBUSTIBLE = w.DESCRIPCION,
                    COSTO_LITRO_DESC = "$" + ((double)w.COSTO_LITRO).ToString("") + " MXN"
                }).OrderBy(w => w.ID_TIPO_COMBUSTIBLE);

                var listado = query != null ? query.ToList() : new List<mTipoCombustible>();

                return listado;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
