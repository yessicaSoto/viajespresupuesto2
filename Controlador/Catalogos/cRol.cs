﻿using Controlador.Tools;

using DBViajesP;
using POCOS.Catalogos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace Controlador.Catalogos
{
    public class cRol : EntityManagerServer<ROLES>
    {
       

        public List<mRol> Listado()
        {
            try
            {
                var query = GetData().Select(w => new mRol()
                {
                    ID_ROL = w.ID_ROL,
                    ROL = w.ROL
                }).OrderBy(w => w.ID_ROL);

                var listado = query != null ? query.ToList() : new List<mRol>();

                return listado;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
