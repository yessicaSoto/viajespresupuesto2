﻿using Controlador.Tools;

using DBViajesP;
using POCOS.Catalogos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace Controlador.Catalogos
{
    public class cViajes : EntityManagerServer<VIAJES>
    {
        

        public List<mViaje> Listado()
        {
            try
            {
                var query = GetData().Select(w => new mViaje()
                {
                    ID_VIAJE = w.ID_VIAJE,
                    ES_REDONDO = w.ES_REDONDO,
                    CANTIDAD_PASEJEROS = w.CANT_PASAJEROS,
                    FECHA_PARTIDAD = w.FECHA_PARTIDA,
                    FECHA_RETORNO = w.FECHA_RETORNO,
                    HORA_PARTIDAD = w.HORA_PARTIDA,
                    HORA_RETORNO = w.HORA_RETORNO
                }).OrderBy(w => w.ID_VIAJE);

                var listado = query != null ? query.ToList() : new List<mViaje>();

                return listado;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
