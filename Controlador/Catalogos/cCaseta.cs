﻿using Controlador.Tools;

using DBViajesP;
using POCOS.Catalogos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace Controlador.Catalogos
{
    public class cCasetas : EntityManagerServer<CASETAS>
    {
      
            public List<mCostoCaseta> ListadoCostosPorRuta(int idRuta)
        {
            try
            {
                Context.Configuration.LazyLoadingEnabled = true;
                var costos = new List<mCostoCaseta>();
                var query = GetData().Where(w=> w.RUTAS.Any(a=> a.ID_RUTA == idRuta)
                && w.COSTO_CASETA.Any()).Select(w => new mCaseta()
                {
                    ID_CASETA = w.ID_CASETA,
                    UBICACION = w.UBICACION, 
                    costoCasetas = w.COSTO_CASETA.Select(a=> new mCostoCaseta {
                        ID_CASETA = a.ID_CASETA,
                        ID_TIPO_TRANSPORTE = a.ID_TIPO_TRANSPORTE,
                        tipoTransporte = a.TIPO_TRANSPORTE.DESCRIPCION,
                        COSTO = (double)a.COSTO
                    }).ToList()

                }).OrderBy(w => w.ID_CASETA);

                var listado = query != null ? query.ToList() : new List<mCaseta>();

                foreach (var i in listado)
                {
                    foreach (var j in i.costoCasetas)
                    {
                        costos.Add(new mCostoCaseta()
                        {
                            ID_CASETA = j.ID_CASETA,
                            ID_TIPO_TRANSPORTE = j.ID_TIPO_TRANSPORTE,
                            tipoTransporte = j.tipoTransporte,
                            COSTO = j.COSTO,
                            caseta = i.UBICACION
                        });
                    }
                }

                return costos;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<mCaseta> ListadoCostos(int idRuta, int idTipoTrans)
        {
            try
            {
                Context.Configuration.LazyLoadingEnabled = true;
                var query = GetData().Where(w => w.RUTAS.Any(a => a.ID_RUTA == idRuta)
                && w.COSTO_CASETA.Any() && w.COSTO_CASETA.Any(a=> a.ID_TIPO_TRANSPORTE == idTipoTrans)).Select(w => new mCaseta()
                {
                    ID_CASETA = w.ID_CASETA,
                    NOMBRE = w.NOMBRE,
                    UBICACION = w.UBICACION,


                }).OrderBy(w => w.ID_CASETA);

                var listado = query != null ? query.ToList() : new List<mCaseta>();

                foreach (var i in listado)
                {
                    i.COSTO = Context.COSTO_CASETA.Any(f => f.ID_CASETA == i.ID_CASETA) ?
                        (Double)Context.COSTO_CASETA.FirstOrDefault(f => f.ID_CASETA == i.ID_CASETA).COSTO
                        : (Double)0;
                }

                return listado;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<mCaseta> Listado()
        {
            try
            {
                Context.Configuration.LazyLoadingEnabled = true;
                var query = GetData().Select(w => new mCaseta()
                {
                    ID_CASETA = w.ID_CASETA,
                     NOMBRE= w.NOMBRE,
                     UBICACION = w.UBICACION,
                     RUTAS = w.RUTAS.Select(c => new mRuta()
                     {
                         ID_RUTA = c.ID_RUTA,
                         RUTA = c.NOMBRE
                     }).ToList(),

                }).OrderBy(w => w.ID_CASETA);

                var listado = query != null ? query.ToList() : new List<mCaseta>();

                foreach (var i in listado)
                {
                    i.RUTASDESC = i.RUTAS.Any() ?
                        string.Join(", ", i.RUTAS.Select(c => c.RUTA).ToList()) : string.Empty;
                }

                return listado;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<mCostoCaseta> ListadoCostosCantidadYRuta(int idRuta, int cantidad)
        {
            try
            {
                Context.Configuration.LazyLoadingEnabled = true;
                var unidades = Context.UNIDADES.Where(w => w.ESTA_DISPONIBLE== true).Select(w => new mUnidad()
                {
                    ID_UNIDAD = w.ID_UNIDAD,
                    CAPACIDAD = w.CAPACIDAD,
                    ID_TRANSPORTE = w.ID_TIPO_TRANSPORTE,
                    ESTA_DISPONIBLE = w.ESTA_DISPONIBLE
                }).OrderBy(w => w.CAPACIDAD);
                List<mUnidad> unidadesSeleccionadas = new List<mUnidad>();
                var costos = new List<mCostoCaseta>();
                var costosPorRuta = ListadoCostosPorRuta(idRuta);
                int restante = cantidad;
                do
                {
                    mUnidad unidad = new mUnidad() { };
                    if (Context.UNIDADES.Any(w => w.CAPACIDAD >= restante && w.ESTA_DISPONIBLE == true))
                    {

                        unidad = Context.UNIDADES.Select(w => new mUnidad()
                        {
                            ID_UNIDAD = w.ID_UNIDAD,
                            CAPACIDAD = w.CAPACIDAD,
                            ID_TRANSPORTE = w.ID_TIPO_TRANSPORTE
                        }).FirstOrDefault(w => w.CAPACIDAD >= cantidad);

                    }
                    else
                    {
                        int max = Context.UNIDADES.Where(w=> w.ESTA_DISPONIBLE == true).Select(w => w.CAPACIDAD).Max();
                        unidad = Context.UNIDADES.Select(w => new mUnidad()
                        {
                            ID_UNIDAD = w.ID_UNIDAD,
                            CAPACIDAD = w.CAPACIDAD,
                            ID_TRANSPORTE = w.ID_TIPO_TRANSPORTE
                        }).FirstOrDefault(w => w.CAPACIDAD == max && w.ESTA_DISPONIBLE == true);

                        unidades.FirstOrDefault(w => w.ID_UNIDAD == unidad.ID_UNIDAD).ESTA_DISPONIBLE = false;
                    }
                    unidadesSeleccionadas.Add(unidad);
                    restante = restante - unidad.CAPACIDAD;
                } while (restante > 0);

                foreach(var i in unidadesSeleccionadas)
                {
                   var precio = costosPorRuta.FirstOrDefault(w => w.ID_TIPO_TRANSPORTE == i.ID_TRANSPORTE);
                   
                        costos.Add(new mCostoCaseta()
                        {
                            ID_CASETA = precio!= null ? precio.ID_CASETA : 0,
                            ID_TIPO_TRANSPORTE = i.ID_TRANSPORTE,
                            tipoTransporte = precio != null ? precio.tipoTransporte : string.Empty,
                            COSTO = precio != null ? precio.COSTO : 0,
                            caseta = precio != null ? precio.caseta : string.Empty,
                            unidad = i.ID_UNIDAD.ToString()
                            
                        });
                    
                }


               
                return costos;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
