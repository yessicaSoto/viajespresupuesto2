﻿using Controlador.Tools;

using DBViajesP;
using POCOS.Catalogos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace Controlador.Catalogos
{
    public class cCiudad : EntityManagerServer<CIUDADES>
    {
       public List<mCiudad> Listado()
        {
            try
            {
                var query = GetData().Select(w => new mCiudad()
                {
                    ID_CIUDAD = w.ID_CIUDAD,
                    CIUDAD = w.CIUDAD,
                   ES_DESTINO = w.ES_DESTINO,
                   ES_SALIDA = w.ES_SALIDA
                }).OrderBy(w => w.ID_CIUDAD);

                var listado = query != null ? query.ToList() : new List<mCiudad>();

                return listado;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
