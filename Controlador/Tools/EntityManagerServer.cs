﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DBViajesP;
using System.Data.Entity;

namespace Controlador.Tools
{
    public class EntityManagerServer<TEntity> where TEntity : class
    {
        #region Global Variables
        private Contexto context;
        public Contexto Context
        {
            get { return context; }
        }
        //internal ObjectSet<TEntity> ObjectSet;
        protected DbSet<TEntity> DbSet;
        #endregion Global Variables

        #region Constructor
        /// <summary>
        /// Constructor que inicializa el contexto con un otro ya existente
        /// </summary>
        /// <param name="contexto">contexto necesario para inicializar otro</param>
        public EntityManagerServer(Contexto contexto)
        {
            this.context = contexto;
            this.DbSet = this.context.Set<TEntity>();//CreateObjectSet<TEntity>();
            this.context.Configuration.LazyLoadingEnabled = false;
        }
        /// <summary>
        /// Constructor que inicializa el contexto con los datos predefinidos
        /// </summary>
        public EntityManagerServer()
        {
            this.context = new Contexto();
            this.DbSet = this.context.Set<TEntity>();//CreateObjectSet<TEntity>();
        }

        /// <summary>
        /// Constructor que inicializa el contexto con los datos predefinidos
        /// </summary>
        /*public EntityManagerServer(string nameOrConnectionString)
        {
            this.context = new SC3Entities(nameOrConnectionString);
            this.DbSet = this.context.Set<TEntity>();//CreateObjectSet<TEntity>();
        }*/

        #endregion Constructor

        #region Methods
        /// <summary>
        /// Obtiene la información del objeto que se mande según el id que reciba
        /// </summary>
        /// <param name="id">id a buscar</param>
        public virtual TEntity GetSingle(Func<TEntity, bool> filter)
        {
            return DbSet.SingleOrDefault(filter);
        }
        /// <summary>
        /// Obtiene la información del objeto que sele mande
        /// </summary>
        /// <param name="filter">filtros que de aplicaran en la búsqueda</param>
        /// <param name="orderBy">ordenamiento de la información</param>
        /// <param name="includeProperties">Nombre de los objetos que se incluiran en la busqueda</param>
        public virtual IQueryable<TEntity> GetData(
            System.Linq.Expressions.Expression<Func<TEntity, bool>> filter = null,
            Func<TEntity, string> orderBy = null,
            string includeProperties = "")
        {
            IQueryable<TEntity> qQuery;
            if (filter != null)
                qQuery = DbSet.Where(filter).AsQueryable();
            else
                qQuery = DbSet;
            if (!string.IsNullOrWhiteSpace(includeProperties))
            {
                foreach (var includeProperty in includeProperties.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
                    qQuery = qQuery.Include(includeProperty.Trim());
            }

            return qQuery;
        }
        /// <summary>
        /// Agrega la información del objeto que se mande
        /// </summary>
        /// <param name="entity">Objeto a guardar</param>        
        public virtual bool Insert(TEntity entity)
        {
            DbSet.Add(entity);

            if (!context.SaveChanges().Equals(0))
                return true;
            else
            {
                return false;
                //throw new Exception("");
            }
        }

        /// <summary>
        /// Agrega la información del objeto que se mande
        /// </summary>
        /// <param name="entity">Objeto a guardar</param>        
        public virtual bool Insert(TEntity entity, out TEntity entidad)
        {
            entidad = DbSet.Add(entity);

            if (!context.SaveChanges().Equals(0))
                return true;
            else
            {
                return false;
                //throw new Exception("");
            }
        }

        /// <summary>
        /// Inserta un listado de entidades si marca error automaticamente no se insertan los otros registros (transacciones)
        /// </summary>
        /// <param name="lstEntities"></param>
        /// <returns></returns>
        public virtual bool Insert(List<TEntity> lstEntities)
        {
            foreach (var entity in lstEntities)
            {
                DbSet.Add(entity);
            }

            if (!context.SaveChanges().Equals(0))
                return true;
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Actualizar la información del objeto que se mande, se necesita todos los parametros
        /// </summary>
        /// <param name="entityToUpdate">Objeto a Actualizar</param>
        public virtual bool Update(TEntity entityToUpdate)
        {

            DbSet.Attach(entityToUpdate);
            context.Entry(entityToUpdate).State = EntityState.Modified;

            if (!context.SaveChanges().Equals(0))
                return true;
            else
            {
                return false;
                throw new Exception("");
            }
        }

        /// <summary>
        /// Actualizar la información del objeto que se mande, se necesita todos los parametros, el segundo parametro son los campos que deseamos que no se actualicen
        /// </summary>
        /// <param name="entityToUpdate"></param>
        /// <param name="excludeFields"></param>
        /// <returns></returns>
        public virtual bool Update(TEntity entityToUpdate, params string[] excludeFields)
        {
            DbSet.Attach(entityToUpdate);
            var entry = context.Entry(entityToUpdate);

            if (excludeFields.Length > 0)
            {
                foreach (var item in excludeFields)
                {
                    entry.Property(item).IsModified = false;
                }
            }

            entry.State = EntityState.Modified;

            return context.SaveChanges() > 0;
        }


        /// <summary>
        /// Actualizar la información del objeto que se mande
        /// </summary>
        /// <param name="entityToUpdate">Objeto a Actualizar</param>
        public virtual bool Update(List<TEntity> entityToUpdate)
        {
            foreach (var item in entityToUpdate)
            {
                DbSet.Attach(item);
                context.Entry(item).State = EntityState.Modified;
            }

            if (!context.SaveChanges().Equals(0))
                return true;
            else
            {
                return false;
                throw new Exception("");
            }
        }

        /// <summary>
        /// Borra el objeto que concuerde con el id
        /// </summary>
        /// <param name="id">id del objeto a borrar</param>
        public virtual bool Delete(Func<TEntity, bool> filter)
        {
            TEntity entityToDelete = GetSingle(filter);
            if (Delete(entityToDelete))
                return true;
            else
            {
                return false;
                throw new Exception("");
            }
        }
        /// <summary>
        /// Borra la información del objeto que se mande
        /// </summary>
        /// <param name="entityToDelete">Objeto a Borrar</param>
        public virtual bool Delete(TEntity entityToDelete)
        {
            //DbSet.Attach(entityToDelete);
            //context.Entry(entityToDelete).State = EntityState.Deleted;
            DbSet.Remove(entityToDelete);
            return context.SaveChanges() > 0;
            //if (!context.SaveChanges().Equals(0))
            //  return true;
            /*else
            {
                return false;
                throw new Exception("");
            }*/
        }

        /// <summary>
        /// Borra los registros de un listado determinado
        /// </summary>
        /// <param name="entityToDelete"></param>
        /// <returns></returns>
        public virtual bool Delete(List<TEntity> entityToDelete)
        {

            foreach (var item in entityToDelete)
            {
                //DbSet.Remove(item);
                DbSet.Attach(item);
                context.Entry(item).State = EntityState.Deleted;
            }

            return context.SaveChanges() > 0;
        }


        /// <summary>
        /// Concatena 2 filtros del mismo tipo con expresion and (&&)
        /// </summary>
        /// <param name="funcIzq">primer filtro a validar</param>
        /// <param name="FuncDer">segundo filtro a validar</param>
        /// <returns>resultado de la concatenacion de los filtros</returns>
        public virtual Func<TEntity, bool> ConcatenarFiltros(Func<TEntity, bool> funcIzq, Func<TEntity, bool> FuncDer)
        {
            return tEntity => funcIzq(tEntity) && FuncDer(tEntity);
        }
        /// <summary>
        /// Concatena varios filtros del mismo tipo con expresion and (&&)
        /// </summary>
        /// <param name="funcs">lista de filtros a concatenar</param>
        /// <returns>resultado de la concatenacion de los filtros</returns>
        public virtual Func<TEntity, bool> ConcatenarFiltros(List<Func<TEntity, bool>> funcs)
        {
            Func<TEntity, bool> funcConatenada = tEntity => true;
            foreach (Func<TEntity, bool> func in funcs)
                funcConatenada = ConcatenarFiltros(funcConatenada, func);
            return funcConatenada;
        }
        #endregion Methods

        /// <summary>
        /// Metodo que regresa informacion de tipo IEnumerable, no es recomendable utilizarlo
        /// </summary>
        /// <param name="filter"></param>
        /// <param name="orderBy"></param>
        /// <param name="includeProperties"></param>
        /// <returns></returns>
        public virtual IEnumerable<TEntity> GetDataEnumerable(
          Func<TEntity, bool> filter = null,
          Func<TEntity, string> orderBy = null,
          string includeProperties = "")
        {
            IQueryable<TEntity> qQuery;
            if (filter != null)
                qQuery = DbSet.Where(filter).AsQueryable();
            else
                qQuery = DbSet;
            if (!string.IsNullOrWhiteSpace(includeProperties))
            {
                foreach (var includeProperty in includeProperties.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
                    qQuery = qQuery.Include(includeProperty.Trim());
            }
            if (orderBy != null)
                return qQuery.OrderBy(orderBy).ToList();
            else
                return qQuery.ToList();
        }

        public virtual IEnumerable<TEntity> GetDataM(
             Func<TEntity, bool> filter = null,
             Func<TEntity, string> orderBy = null,
             string includeProperties = "")
        {
            IQueryable<TEntity> qQuery;
            if (filter != null)
                qQuery = DbSet.Where(filter).AsQueryable();
            else
                qQuery = DbSet;
            if (!string.IsNullOrWhiteSpace(includeProperties))
            {
                foreach (var includeProperty in includeProperties.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
                    qQuery = qQuery.Include(includeProperty.Trim());
            }
            if (orderBy != null)
                return qQuery.OrderBy(orderBy).ToList();
            else
                return qQuery.ToList();
        }

        /// <summary>
        /// Metodo que retorna true si existe la base de datos
        /// </summary>
        /// <returns></returns>
        public bool Exists()
        {
            return context.Database.Exists();
        }

        ///
        public virtual int GetNextId(Func<TEntity, int> filter)
        {
            var id = DbSet.Any() ? DbSet.Max(filter) : 0;
            return id + 1;
        }

        public virtual void UseLazyLoad(bool usarLazyLoad = false)
        {
            context.Configuration.LazyLoadingEnabled = usarLazyLoad;
        }
    }

}
