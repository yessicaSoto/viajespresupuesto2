﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using POCOS.Catalogos;

namespace ViajesPresupuesto.Models.Catalogos
{
    public class TipoCombustibleViewModel
    {
        public int ID_TIPO_COMBUSTIBLE { get; set; }
        public string TIPO_COMBUSTIBLE { get; set; }
    }
}