﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using POCOS.Catalogos;

namespace ViajesPresupuesto.Models.Catalogos
{
    public class RutaViewModel
    {
        public int ID_RUTA { get; set; }
        public string RUTA { get; set; }
    }
}