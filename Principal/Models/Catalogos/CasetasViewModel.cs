﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using POCOS.Catalogos;

namespace ViajesPresupuesto.Models.Catalogos
{
    public class CasetaViewModel
    {
        public int ID_CASETA { get; set; }
        public string NOMBRE { get; set; }
    }
}