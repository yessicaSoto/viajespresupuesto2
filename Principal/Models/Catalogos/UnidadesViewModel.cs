﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using POCOS.Catalogos;

namespace ViajesPresupuesto.Models.Catalogos
{
    public class UnidadViewModel
    {
        public int ID_UNIDAD { get; set; }
        public string UNIDAD { get; set; }
    }
}