jQuery.Validacion = {
    LimpiarFomulario: function () {
        $('.input-validation-error').addClass('valid');
        document.querySelectorAll('.valid').forEach(function (el) {
            el.setAttribute("aria-invalid", "false");
        });
        document.querySelectorAll('.field-validation-error').forEach(function (el) {
            el.innerHTML = "";
        });
        $('.input-validation-error').removeClass('input-validation-error');
        //removing validation message from  tag created dynamically
        $('.field-validation-error').addClass('field-validation-valid');
        $('.field-validation-error').removeClass('field-validation-error');
        $('.validation-summary-errors').addClass('validation-summary-valid');
        $('.validation-summary-errors').removeClass('validation-summary-errors');
    },
}