﻿using System.Web;
using System.Web.Optimization;

namespace Principal
{
    public class BundleConfig
    {
        // Para obtener más información sobre las uniones, visite https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery")
                .Include("~/plugins/jquery/jquery.min.js"));

            /*vue*/
            bundles.Add(new ScriptBundle("~/bundles/vue")
                .Include("~/Scripts/vue.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/inputmask")
                .Include("~/Scripts/inputmask/inputmask.js")
                .Include("~/Scripts/inputmask/jquery.inputmask.js"));

            bundles.Add(new ScriptBundle("~/bundles/adminlte")
                   .Include("~/dist/js/adminlte.min.js")
                   .Include("~/dist/js/demo.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval")
                .Include("~/Scripts/jquery.validate.min.js")
                .Include("~/Scripts/jquery.validate.unobtrusive.min.js")
                .Include("~/Scripts/jquery.validate.custom.js")
                .Include("~/Scripts/jquery.validate.form.js"));

           

            /*axios*/
            bundles.Add(new ScriptBundle("~/bundles/axios")
                .Include("~/Scripts/axios.min.js")
                .Include("~/Scripts/axios.interceptor.js"));

            /*Growl*/
            bundles.Add(new ScriptBundle("~/bundles/growl")
                .Include("~/Scripts/jquery.growl.js"));

            /*Datattable*/
            bundles.Add(new ScriptBundle("~/bundles/datatable")
                .Include("~/plugins/datatables/jquery.dataTables.min.js")
                .Include("~/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js")
                .Include("~/plugins/datatables-responsive/js/dataTables.responsive.min.js")
                .Include("~/plugins/datatables-responsive/js/responsive.bootstrap4.min.js")
                .Include("~/plugins/datatables/jquery.dataTables.Tool.js"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at https://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/boostrap").Include(
                      "~/plugins/bootstrap/js/bootstrap.bundle.min.js"));

            /*utilerias*/
            bundles.Add(new ScriptBundle("~/bundles/utilerias")
                .Include("~/plugins/utilerias/utilerias.js"));

            bundles.Add(new StyleBundle("~/Content/css")
                .Include("~/Content/bootstrap.css")
                .Include("~/Content/site.css"));

            /*Font Awesome*/
            bundles.Add(new StyleBundle("~/Content/FontAwesome").Include(
                     "~/plugins/fontawesome-free/css/all.min.css", new CssRewriteUrlTransform()));
            /*adminlte*/
            bundles.Add(new StyleBundle("~/Content/adminlte").Include(
                     "~/dist/css/adminlte.min.css", new CssRewriteUrlTransform()));
            /*Growl*/
            bundles.Add(new StyleBundle("~/Content/growl").Include(
                   "~/Content/jquery.growl.css", new CssRewriteUrlTransform()));

            bundles.Add(new ScriptBundle("~/bundles/login_scripts")
                .Include("~/Assets/Vendor/jquery/jquery-1.11.1.min.js")
                .Include("~/Assets/Vendor/jquery/jquery_ui/jquery-ui.min.js")
                .Include("~/Assets/Vendor/canvasbg/canvasbg.js")
                .Include("~/Assets/js/utility/utility.js")
                .Include("~/Assets/js/main.js")
                .Include("~/Assets/Vendor/validate/jquery.validate.min.js")
                .Include("~/Assets/Vendor/gritter/jquery.gritter.min.js")
                .Include("~/Assets/Scripts/vue.js")
                .Include("~/Assets/Scripts/axios.min.js")
                .Include("~/Assets/plugins/utilerias/utilerias.js"));
            /*"~/vue/login/LoginController.js"));*/

            /*ES6-Promise*/
            bundles.Add(new ScriptBundle("~/bundles/es6-promise")
                .Include("~/assets/vendor/es6-promise/es6-promise.min.js")
                .Include("~/assets/vendor/es6-promise/es6-promise.auto.min.js"));

            /*DataTable*/
            bundles.Add(new StyleBundle("~/Content/datatable")
                .Include("~/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css", new CssRewriteUrlTransform())
                .Include("~/plugins/datatables-responsive/css/responsive.bootstrap4.min.css", new CssRewriteUrlTransform()));

            bundles.Add(new StyleBundle("~/Content/login_estilo")
                .Include("~/Assets/skin/default_skin/css/theme.css", new CssRewriteUrlTransform())
                .Include("~/Assets/admin-tools/admin-forms/css/admin-forms.css", new CssRewriteUrlTransform())
                .Include("~/Assets/Vendor/gritter/jquery.gritter.css", new CssRewriteUrlTransform()));

        }
    }
}
