﻿using Controlador.Catalogos;

using POCOS.Catalogos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ViajesPresupuesto.Controllers.Catalogos
{
    [RoutePrefix("Presupuesto")]
    public class PresupuestoController : Controller
    {
        public PresupuestoController()
        {
        }

        [HttpGet, ActionName("Index")]
        public ActionResult Index()
        {
            return View("~/Views/GenerarPresupuesto/Index.cshtml");
        }
        [HttpGet, ActionName("costosCombustible")]
        public JsonResult costosCombustible(int cantidad, int ruta)
        {
            
            var u = new cUnidades().ObtenerUnidades(cantidad);
            u = new cUnidades().ObtenerCostosCombustible(u);
            var distancia = new cRutas().ObtenerRuta(ruta);

            foreach (var i in u)
            {
                i.kilometraje = (distancia.DISTANCIA * 3) / i.costoCombustible;
            }

            return Json(u, JsonRequestBehavior.AllowGet);
        }
        [HttpGet, ActionName("Generar")]
        public JsonResult Generar(int ciudadOrigen, int ciudadDestino, string fSalida, string hSalida, string fRegreso, string hRegreso, int ruta, int cPasajeros, bool esRedondo)
        {
            var Ruta = new cRutas().ObtenerRuta(ruta);
            int cantidadUnidades = 1;
            var viaje = new mViaje
            {
                ruta = Ruta.destinosDesc,
                nombreRuta = Ruta.RUTA,
                fechaPar = fSalida,
                horaPar = hSalida,
                FechaRe = fRegreso,
                horRe = hRegreso
            };
            var peajes = new cCasetas().ListadoCostosCantidadYRuta(ruta, cPasajeros);

            var totalPeaje = peajes.Sum(item => item.COSTO);

            var u = new cUnidades().ObtenerUnidades(cPasajeros);
            //u = new cUnidades().ObtenerCostosCombustible(u);
            var distancia = new cRutas().ObtenerRuta(ruta);

            //foreach (var i in u)
            //{
            //    i.kilometraje = (distancia.DISTANCIA * 3) / i.costoCombustible;
            //}

            cantidadUnidades = u.Count();

            DateTime fInicio = DateTime.Parse(fSalida);
            DateTime fFin = DateTime.Parse(fRegreso);
            var dias = (fFin - fInicio).Days;

            var viaticos = cantidadUnidades * 123.22 * dias;
            var totalCombustible = u.Sum(i => i.kilometraje);
            var presupuesto = new mPresupuesto();
            presupuesto.DatosGenerales = viaje;
            presupuesto.peajes = peajes;
            presupuesto.totalPeajes = totalPeaje;
            presupuesto.viaticos = viaticos;
            presupuesto.total = totalPeaje + viaticos + totalCombustible;
            return Json(presupuesto, JsonRequestBehavior.AllowGet);
        }

        //[HttpGet, ActionName("ComboBox")]
        //public JsonResult ComboBox()
        //{
        //    var _listado = _ruta.ComboBox();
        //    return Json(_listado, JsonRequestBehavior.AllowGet);
        //}
    }
}