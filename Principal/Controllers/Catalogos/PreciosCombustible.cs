﻿using Controlador.Catalogos;

using POCOS.Catalogos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ViajesPresupuesto.Controllers.Catalogos
{
    [RoutePrefix("TipoCombustible")]
    public class TipoCombustibleController : Controller
    {
        public TipoCombustibleController()
        {
        }
 
        [HttpGet, ActionName("Index")]
        public ActionResult Index()
        {
            return View("~/Views/Catalogos/TipoCombustible/Index.cshtml");
        }

        [HttpGet, ActionName("Listado")]
        public JsonResult Listado()
        {
            var listado = new /*cTipoCombustiblees().Listado();*/ List<mTipoCombustible>();
            return Json(listado, JsonRequestBehavior.AllowGet);
        }

    }
}