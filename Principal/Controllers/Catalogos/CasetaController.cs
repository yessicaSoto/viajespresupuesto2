﻿using Controlador.Catalogos;

using POCOS.Catalogos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ViajesPresupuesto.Controllers.Catalogos
{
    [RoutePrefix("Caseta")]
    public class CasetaController : Controller
    {
        public CasetaController()
        {
        }

        [HttpGet, ActionName("Index")]
        public ActionResult Index()
        {
            return View("~/Views/Catalogos/Caseta/Index.cshtml");
        }

        [HttpGet, ActionName("Listado")]
        public JsonResult Listado()
        {
            var listado = new cCasetas().Listado();
            return Json(listado, JsonRequestBehavior.AllowGet);
        }
        [HttpGet, ActionName("ListadoCostos")]
        public JsonResult ListadoCostos(int idRuta, int idTrans)
        {
            var listado = new cCasetas().ListadoCostos(idRuta, idTrans);
            return Json(listado, JsonRequestBehavior.AllowGet);
        }
        [HttpGet, ActionName("ListadoCostosPorRuta")]
        public JsonResult ListadoCostosPorRuta(int idRuta)
        {
            var listado = new cCasetas().ListadoCostosPorRuta(idRuta);
            return Json(listado, JsonRequestBehavior.AllowGet);
        }
        [HttpGet, ActionName("ListadoCostosCantidadYRuta")]
        public JsonResult ListadoCostosCantidadYRuta(int idRuta, int cantidad)
        {
            var listado = new cCasetas().ListadoCostosCantidadYRuta(idRuta, cantidad);
            return Json(listado, JsonRequestBehavior.AllowGet);
        }



    }

}