﻿using Controlador.Catalogos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Principal.Controllers.Catalogos
{
    [RoutePrefix("Ciudades")]
    public class CiudadesController : Controller
    {
        public CiudadesController()
        {
        }

        //[HttpGet, ActionName("Index")]
        //public ActionResult Index()
        //{
        //    return View("~/Views/Catalogos/Ciudades/Index.cshtml");
        //}

        [HttpGet, ActionName("Listado")]
        public JsonResult Listado()
        {
            var listado = new cCiudad().Listado();
            return Json(listado, JsonRequestBehavior.AllowGet);
        }

    }
}