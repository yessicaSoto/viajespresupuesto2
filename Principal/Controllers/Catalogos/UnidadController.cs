﻿using Controlador.Catalogos;

using POCOS.Catalogos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ViajesPresupuesto.Controllers.Catalogos
{
    [RoutePrefix("Unidad")]
    public class UnidadController : Controller
    {
        public UnidadController()
        {
        }
 
        [HttpGet, ActionName("Index")]
        public ActionResult Index()
        {
            return View("~/Views/Catalogos/Unidad/Index.cshtml");
        }

        [HttpGet, ActionName("Listado")]
        public JsonResult Listado()
        {
            var listado = new cUnidades().Listado();
            return Json(listado, JsonRequestBehavior.AllowGet);
        }

    }
}