﻿using Controlador.Catalogos;

using POCOS.Catalogos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ViajesPresupuesto.Controllers.Catalogos
{
    [RoutePrefix("TipoTransporte")]
    public class TipoTransporteController : Controller
    {
        public TipoTransporteController()
        {
        }
 
        [HttpGet, ActionName("Index")]
        public ActionResult Index()
        {
            return View("~/Views/Catalogos/TipoCombustible/Index.cshtml");
        }

        [HttpGet, ActionName("Listado")]
        public JsonResult Listado()
        {
            var listado = new cCostosCombustible().Listado();
            return Json(listado, JsonRequestBehavior.AllowGet);
        }

    }
}