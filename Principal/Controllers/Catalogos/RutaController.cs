﻿using Controlador.Catalogos;

using POCOS.Catalogos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ViajesPresupuesto.Controllers.Catalogos
{
    [RoutePrefix("Ruta")]
    public class RutaController : Controller
    {
        public RutaController()
        {
        }
          
        [HttpGet, ActionName("Index")]
        public ActionResult Index()
        {
            return View("~/Views/Catalogos/Ruta/Index.cshtml");
        }

        [HttpGet, ActionName("Listado")]
        public JsonResult Listado()
        {
            var listado = new cRutas().Listado();
            return Json(listado, JsonRequestBehavior.AllowGet);
        }
        [HttpGet, ActionName("ListadoConSalidaYDestino")]
        public JsonResult ListadoConSalidaYDestino(int salida, int destino)
        {
            var listado = new cRutas().ListadoConSalidaYDestino(salida, destino);
            return Json(listado, JsonRequestBehavior.AllowGet);
        }
        
        //[HttpGet, ActionName("ComboBox")]
        //public JsonResult ComboBox()
        //{
        //    var _listado = _ruta.ComboBox();
        //    return Json(_listado, JsonRequestBehavior.AllowGet);
        //}
    }
}